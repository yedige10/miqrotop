/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
//require('./bootstrap');
import App from './components/App'
import VueResource from 'vue-resource'
import router from './components/router'
import auth from './components/auth'
window.Vue = require('vue');
Vue.use(auth);
import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use( CKEditor );
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);
import VModal from 'vue-js-modal'
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
import Element from 'element-ui'
Vue.use(VueResource)
Vue.use(Element)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
