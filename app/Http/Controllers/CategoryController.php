<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Category;
use App\Quote;
use App\Materialimage;
use File;
class CategoryController extends Controller
{
    public function deleteCategory($id){
        $quotes=DB::table('quotes')->where('category_id', $id)->get();
        for($i=0;$i<sizeof($quotes);$i++){
            Materialimage::where('material_id',$quotes[$i]->id)->delete();
            File::delete($quotes[$i]->images);
        }        
        
        Quote::where('category_id', $id)->delete();
        Category::where('id', $id)->delete();
        
        return response()->json([
            'success' => true,
            'msg'=>'deleted'
        ]);
    }
    public function postCategory(Request $request){
        $content=$request->content;
        $category = Category::create([
            'name' => $content['name'],
        ]);
        
        return response()->json([
            'category' => $category,
            'success'=>true
        ], 200);
    
    }
    public function getCategory(){
        $category=DB::select('select*from categories');
        return response()->json([
            'category' => $category
        ], 200);
    }
    public function putCategory(Request $request,$id){
        $category = Category::find($id);
        if (!$category) {
            return response()->json(['message' => 'category not found'], 404);
        }
        
        $category->name = $request->content['editName'];
        $category->save();
        
        return response()->json([
            'success' => true,
            'msg'=>'updated'
        ]);
    }

}