<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/project', 'ProjectController@index');
Route::post('/quote',[
    'uses'=>'QuoteController@postQuote',
    'middleware'=>'auth.jwt'
]);
Route::post('/category',[
    'uses'=>'CategoryController@postCategory',
    'middleware'=>'auth.jwt'
]);
Route::get('/category',[
    'uses'=>'CategoryController@getCategory',
    'middleware'=>'auth.jwt'
]);

Route::delete('/category/{id}',[
    'uses'=>'CategoryController@deleteCategory',
    'middleware'=>'auth.jwt'
]);
Route::get('/quotes',[
    'uses'=>'QuoteController@getQuotes',
    'middleware'=>'auth.jwt'
]);
Route::get('/usercategory',[
    'uses'=>'QuoteController@getUserCategory',
    
]);
Route::get('/userquotes',[
    'uses'=>'QuoteController@getUserQuotes',
    
]);
Route::get('/single/{id}',[
    'uses'=>'QuoteController@getSingleQuote',
    
]);

Route::put('/quote/{id}',[
    'uses'=>'QuoteController@putQuote',
    'middleware'=>'auth.jwt'
]);

Route::put('/category/{id}',[
    'uses'=>'CategoryController@putCategory',
    'middleware'=>'auth.jwt'
]);
Route::delete('/quote/{id}',[
    'uses'=>'QuoteController@deleteQuote',
    'middleware'=>'auth.jwt'
]);
Route::post('/user',[
    'uses'=>'UserController@signup'
]);
Route::post('/send',[
    'uses'=>'UserController@send'
]);
Route::post('user/signin',[
    'uses'=>'UserController@signin'
]);