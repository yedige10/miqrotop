<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materialimage extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'material_id',
        'images'
    ];
}
