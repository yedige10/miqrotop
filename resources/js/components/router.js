/* eslint-disable */

import Vue from "vue";
import Router from "vue-router";
import newMaterial from "../components/newMaterial.vue";
import Signin from "../components/signin.vue";
import Materials from "../components/materials.vue";
import AddCategory from "../components/newCategory.vue";
import Home from "../components/home.vue";
import Admin from "../components/adminPage";
import Category from "../components/category";
import Single from '../components/singleMaterial'
Vue.use(Router);
const routes = [
    {
        path: "/",
        component: Home
    },
    {
        path:'/single/:id',
        name:'single',
        component:Single,
        props:true
    },
    {
        path: "/admin",
        component: Admin,
        meta: {
            forAdmin: true
        },
        children: [
            { path: "materials", name: "materials", component: Materials },
            { path: "categories", name: "categories", component: Category },
            {
                path: "new-category",
                name: "newCategory",
                component: AddCategory
            },
            {path:'new-material',name:'newMaterial',component:newMaterial}
        ]
    },
    {
        path: "/signin",
        component: Signin,
        name:'login',
        meta: {
            forVisitors: true
        }
    }
];

const router = new Router({
    mode: "history",
    routes: routes
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitors)) {
        if (Vue.auth.isAuthenticated()) {
            next({
                path: "/admin"
            });
        } else next();
    } else next();
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.forAdmin)) {
        if (!Vue.auth.isAuthenticated()) {
            next({
                path: "/signin"
            });
        } else next();
    } else next();
});

export default router;
