<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Mail;
class UserController extends Controller{
    public function send(Request $request){
        $to_name = 'Yerzhan aga';
        $to_email = 'miqrotop@gmail.com';
        $data = array('name'=>  'Yedige', 'body' => 'A test mail');
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('Laravel Test Mail');
        $message->from('yedigebaltabekov@gmail.com','Test Mail');
});
    }
    public function signup(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required',
        ]);
        $user=new User([
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'password'=>bcrypt($request->input('password'))
        ]);
        $user->save();
        return response()->json([
            'message'=>'Successfully created user'
        ],201);
    }

    public function signin(Request $request){
        $this->validate($request,[  
            'email'=>'required|email',
            'password'=>'required',
        ]);
        $credentials=$request->only('email','password');
        
        try{
            if(!$token = JWTAuth::attempt($credentials)){
                return response()->json([
                    'error'=>'Invalid crediantals'
                ],401);
            }
        } catch (JWTException $e){
            return response()->json(['error'=>'counld not create token'],500);
        }
        return response()->json(['token'=>$token
    ],200);
    }
}