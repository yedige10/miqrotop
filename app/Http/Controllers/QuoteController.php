<?php
namespace App\Http\Controllers;
use App\Quote;
use App\Materialimage;
use File;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
class QuoteController extends Controller{

    public function postQuote(Request $request){
        $name=$request->content['name'];
        $images=$request->content['image'];
        $content = $request->content;
        $quote = Quote::create([
            'name'=>$name,
            'content' => $content['quoteContent'],
            'user_id' => auth()->user()->id,
          //  'image'=>$filename,
            'category_id'=>$content['category']
        ]); 
        for($i=0;$i<sizeof($images);$i++){
            
        $exploded=explode(',',$images[$i]);
        $decoded=base64_decode($exploded[1]);
        if(str_contains($exploded[0],'jpeg')){
            $extension='jpg';
        }
        else{
            $extension='png';
        }
        $filename=str_random().'.'.$extension;
        $path=public_path().'/'.$filename;
        file_put_contents($path,$decoded);
        
        
        $user= JWTAuth::parseToken()->toUser();
        $materialImage=Materialimage::create([
            'material_id'=>$quote->id,
            'images'=>$filename
        ]);
    }
        

        return response()->json([
            'success'=>true,
            'quote' => $quote,
            'user' => $user,
            'image'=>$materialImage
        ]);
    }
    public function getQuotes(){
        $quotes=DB::select('select quotes.name as qname,quotes.id as qid,quotes.content,categories.name,categories.id as cid from quotes inner join  categories on quotes.category_id=categories.id');
        for($i=0;$i<sizeof($quotes);$i++){
            $images=DB::select('select*from images where material_id='.$quotes[$i]->qid);
            $quotes[$i]->images=$images;
        }
        $response=[
            'quotes'=>$quotes
        ];
        return response()->json($response,200);
    }
    public function getSingleQuote($id){
        $material=Quote::select('category_id')->where('id',$id)->first(); 
        $materials=DB::select('select id,name from quotes where category_id=?',[$material->category_id]);
        $quotes=DB::select('select quotes.name as qname,quotes.id as qid,quotes.content,
        categories.name,categories.id as cid from quotes inner join  
        categories on quotes.category_id=categories.id where quotes.id=?',[$id]);
        for($i=0;$i<sizeof($quotes);$i++){
            $images=DB::select('select*from images where material_id='.$quotes[$i]->qid);
            $quotes[$i]->images=$images;
        }
        $response=[
            'quotes'=>$quotes,
            'materials'=> $materials
        ];
        return response()->json($response,200);
    }
    public function getUserCategory(){
        $categories=DB::select("select*from categories");
       
        $response=[
            'categories'=>$categories
        ];
        return response()->json($response,200);
    }
    public function getUserQuotes(){
        $quotes=DB::select('select quotes.name as qname,quotes.id as qid,quotes.content,categories.name,categories.id as cid from quotes inner join  categories on quotes.category_id=categories.id');
        for($i=0;$i<sizeof($quotes);$i++){
            $images=DB::select('select*from images where material_id='.$quotes[$i]->qid);
            $quotes[$i]->images=$images;
        }
        $response=[
            'userquotes'=>$quotes
        ];
        return response()->json($response,200);
    }
    public function putQuote(Request $request,$id){
        $quote = Quote::find($id);
        if (!$quote) {
            return response()->json(['message' => 'Document not found'], 404);
        }
        $quote->content = $request->input('content');
        $quote->save();
        return response()->json(['quote' => $quote], 200);
    }
    public function deleteQuote($id){
        $images=DB::table('images')->where('material_id', '=', $id)->get();
        foreach($images as $image){
            File::delete($image->images);
        }
            
        
        DB::table('images')->where('material_id', '=', $id)->delete();
        $quote = Quote::find($id);
        
        $quote->delete();
        return response()->json(['message' => 'Quote deleted','success'=>true]);
    }
}